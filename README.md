# PhotoSpaces

## Prerequisitos

Es necesario tener instalada la última versión de NodeJS y la versión 2.92 de Blender (con el .exe como variable en el PATH del sistema).

## Instalación

Clonar el repositorio con:

```bash
git clone https://gitlab.com/JoseSF/photospaces.git
```

### Backend

Desde la carpeta de backend, ejecutar:

```bash
npm install
```

### Frontend

Desde la carpeta de frontend, ejecutar:

```bash
npm install
```

## Ejecución

### Backend

Desde la carpeta de backend, ejecutar:

```bash
npm start
```

Esto iniciará el servidor en el puerto 3030.

### Frontend

Desde la carpeta de frontend, ejecutar:

```bash
npm start
```

Esto iniciará el cliente en el puerto 8080.
